import 'package:flutter/material.dart';
import 'MyAppBar.dart';
import './MyBottomNav.dart';

class MyLayout extends StatelessWidget {
  final Widget body;

  MyLayout(this.body);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(), body: body, bottomNavigationBar: MyBottomNavBar());
  }
}
