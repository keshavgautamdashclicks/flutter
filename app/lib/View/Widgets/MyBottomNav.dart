import 'package:flutter/material.dart';
import '../../util/consts.dart';

class MyBottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      unselectedFontSize: 12,
      selectedFontSize: 15,
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: AppColors.light,
      selectedItemColor: AppColors.light2,
      backgroundColor: AppColors.primary,
      iconSize: 35,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.attach_money),
          title: Text('Transactions'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.assignment),
          title: Text('Planning'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.pie_chart),
          title: Text('Statistics'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.lightbulb_outline),
          title: Text('Tips'),
        ),
      ],
    );
  }
}
