import 'package:flutter/material.dart';
import '../../util/consts.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text('Hello'),
      backgroundColor: AppColors.primary,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
