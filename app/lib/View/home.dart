import 'package:flutter/material.dart';
import './Widgets/MyLayout.dart';
import '../util/consts.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyLayout(
      Container(
        color: AppColors.secondary,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 50,
                vertical: 30,
              ),
              color: AppColors.primary,
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 55,
                    backgroundColor: AppColors.light,
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: AssetImage('assets/images/Juana.png'),
                    ),
                  ),
                  Column(
                    children: [],
                  )
                ],
              ),
            ),
            Column()
          ],
        ),
      ),
    );
  }
}
