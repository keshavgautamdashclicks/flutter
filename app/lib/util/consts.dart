import 'package:hexcolor/hexcolor.dart';

class AppColors {
  static Hexcolor get primary => Hexcolor('#242326');
  static Hexcolor get secondary => Hexcolor('#36363B');
  static Hexcolor get light => Hexcolor('#D4D4D4');
  static Hexcolor get tercerary => Hexcolor('#495F73');
  static Hexcolor get light2 => Hexcolor('#A0D2FF');
}
