import 'package:flutter/material.dart';
import './View/home.dart';
import './util/consts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Finances',
      color: AppColors.primary,
      home: Home(),
    );
  }
}
