# Flutter Finances App

This is a Mobile application build on Dart (Flutter). The main objective of this application is to help people to know 
how they spend their money, how they save money, and give strategies to improve their financial live.

## Installation

First of all you need to install [Flutter](https://flutter.dev/docs/get-started/install/windows). You may have (Apriori) your Android device connected and with [Debugg Mode Activated](https://www.companionlink.com/support/kb/Enable_Android_USB_Debugging_Mode). After that you could run the application using:

```bash
cd flutter
cd app
flutter run
```

## Contributing
Team 14:

Gabriel
Juan
Mateo
Nicolás

## License

MIT License

Copyright (c) 2020 Mateo Contreras

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.